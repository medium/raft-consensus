const winston = require('winston');
const purdy = require('purdy');
const { gray, cyan, red, yellow } = require('chalk');
const moment = require('moment');


const colorScheme = {
  'SILLY': gray,
  'DEBUG': gray,
  'INFO': cyan,
  'WARN': yellow,
  'ERROR': red
};
const colorLevel = level => colorScheme[level](level);

const logLevel = process.env['LOG_LEVEL'] || 'debug';
const currentTime = () => moment().format('HH:mm:ss.SSS');

const stringifyMeta = meta => {
  if (!Object.keys(meta).length) return '';

  const metaString = purdy.stringify(
    meta, { depth: null, indent: 2 }
  );
  return `\n  ${cyan('META')}: ${metaString}`;
};

const formatter = options => {
  const timestamp = options.timestamp();
  const level = options.level.toUpperCase();
  const message = options.message || '';
  const meta = options.meta && stringifyMeta(options.meta);

  return `${gray(`[${timestamp}]`)} ${colorLevel(level)}: ${message} ${meta}`;
};

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      level: logLevel,
      timestamp: currentTime,
      formatter
    })
  ]
});


module.exports = logger;
