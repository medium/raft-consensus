const Redis = require('ioredis');
const { prop } = require('ramda');
const F = require('fluture');

const config = require('../config');
const redisConfig = prop('redis', config);

const methods = ['set', 'mset', 'get', 'keys'];

const createRedisClient = () => {
  const client = new Redis(redisConfig);

  const pipeline = () => {
    const pipe = client.pipeline();

    return {
      exec: () => F.node(done => pipe.exec(done)),
      push: (command, ...args) => pipe[command](...args)
    };
  };

  return methods.reduce((memo, key) => {
    memo[key + 'F'] = (...args) => {
      return F.node(done => client[key](...args, done));
    };
    return memo;
  }, { client, pipeline });
};


module.exports = createRedisClient;
