const { createNode } = require('./lib/node');
const RedisProvider = require('./lib/providers/redis');

const node = createNode({
  Provider: RedisProvider,
  options: {
    electionTimeout: [100, 200],
    heartbeatInterval: 50
  }
});
node.init();

//setTimeout(node.destroy, 4000);
