class NotOverridedMethodError extends Error {
  constructor(name) {
    super(`Method ${name} should be overriden`);
    Error.captureStackTrace(this, NotOverridedMethodError);
  }
}

class Provider {
  constructor(node) {
    this.node = node;
  }
}


module.exports = Provider;
