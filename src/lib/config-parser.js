const fs = require('fs');
const { red } = require('chalk');
const { memoize } = require('ramda');
const { safeLoad } = require('js-yaml');

const {
  encaseEither2_, encaseEither2, encaseEither, I, __, pipe,
  either, chain, prop
} = require('../sanctuary');


const readFileSyncE = encaseEither2_(I, fs.readFileSync, __, 'utf8');
const parseYmlE = encaseEither(prop('message'), safeLoad);
const propE = encaseEither2(prop('message'), prop);

const errorFail = message => {
  console.error(red('ERROR:'), message);
  process.exit(1);
};

const configParser = memoize(configPath => {
  const environment = process.env.NODE_ENV || 'development';
  const configData = pipe([
    readFileSyncE,
    chain(parseYmlE),
    chain(propE(environment))
  ], configPath);

  return either(errorFail, I, configData);
});


module.exports = configParser;
