const { last, split } = require('ramda');
const { yellow } = require('chalk');

const createRedisClient = require('../../db/redis');
const Provider = require('../provider');
const logger = require('../../logger');

const { client } = createRedisClient();
const { client: subscriberClient } = createRedisClient();

const publish = (...args) => client.publish(...args);

class RedisProvider {
  constructor(node) {
    this.node = node;

    logger.info('NODE ID:', yellow(node.id));
    const events = node.events;
    const channels = [
      'vote_offer',
      'applied_offer',
      'rejected_offer',
      'leader_heartbeat'
    ];

    subscriberClient.subscribe(...channels);
    subscriberClient.on('message', (channel, message) => {
      //console.log(channel, message);
      const parsedMessage = JSON.parse(message);

      switch (channel) {
        case 'vote_offer':
          if (node.id !== parsedMessage.nodeId)
            events.emit('voteOffer', parsedMessage);
          break;
        case 'applied_offer':
          if (node.id === parsedMessage)
            events.emit('appliedOffer', parsedMessage);
          break;
        case 'rejected_offer':
          if (node.id === parsedMessage)
            events.emit('rejectedOffer', parsedMessage);
          break;
        case 'leader_heartbeat':
          if (node.id !== parsedMessage.nodeId)
            events.emit('leaderHeartbeat', parsedMessage);
          break;
      }
    });
  }

  announce() {
    const setArgs = [
      `node:${this.node.id}`,
      this.node.fsm.state,
      'PX',
      this.node.options.announceInterval + 50
    ];
    client.set(...setArgs);
  }

  getMembersCount(done) {
    client.keys('node:*', (err, res) => {
      if (err) return done(err);
      done(null, res.length);
    });
  }

  sendVote() {
    const voteOffer = {
      term: this.node.term,
      nodeId: this.node.id
    };
    publish('vote_offer', JSON.stringify(voteOffer));
  }

  applyVoteOffer(nodeId) {
    publish('applied_offer', JSON.stringify(nodeId));
  }

  rejectVoteOffer(nodeId) {
    publish('rejected_offer', JSON.stringify(nodeId));
  }

  leaderHeartbeat() {
    const heartbeat = {
      term: this.node.term,
      nodeId: this.node.id
    };
    publish('leader_heartbeat', JSON.stringify(heartbeat));
  }
}

module.exports = RedisProvider;
