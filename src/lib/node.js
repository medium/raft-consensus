const EventEmitter = require('events');
const uuidv4 = require('uuid/v4');
const { merge } = require('ramda');
const StateMachine = require('javascript-state-machine');
const logger = require('../logger');

const {
  startAnnouce, initElectionTimout, listenLeaderHeartbeat,
  listenVoteOffers, listenForVotes, stopElectionTimeout,
  sendVoteOffers, stopListenForVotes, stopListenVoteOffers,
  stopListenLeaderHeartbeat, initLeaderHeartbeat,
  newElectionTerm
} = require('./node-fns');

const defaultOptions = {
  announceInterval: 300,
  electionTimeout: [300, 500],
  heartbeatInterval: 50
};

const createNode = ({ Provider, options }) => {
  const events = new EventEmitter();
  const id = uuidv4();
  options = merge(defaultOptions, options);

  const node = { id, events, options, term: 0 };

  const fsm = new StateMachine({
    init: 'none',
    transitions: [
      { name: 'initialize', from: 'none', to: 'follower' },
      { name: 'becomeCandidate', from: 'follower', to: 'candidate' },
      { name: 'becomeLeader', from: 'candidate', to: 'leader' },

      { name: 'loseElection', from: 'candidate', to: 'follower' },
      { name: 'permitLeader', from: 'leader', to: 'follower' },

      { name: 'reset', from: '*', to: 'none' }
    ],
    methods: {
      onInitialize: () => {
        startAnnouce(node);
        initElectionTimout(node);

        listenLeaderHeartbeat(node);
        listenVoteOffers(node);
      },
      onBecomeCandidate: () => {
        newElectionTerm(node);
        initElectionTimout(node);

        listenForVotes(node);

        sendVoteOffers(node);
      },
      onBecomeLeader: () => {
        stopListenVoteOffers(node);
        stopListenForVotes(node);
        stopElectionTimeout(node);

        initLeaderHeartbeat(node);
      },

      onLoseElection: () => {
        initElectionTimout(node);
        stopListenForVotes(node);
      },

      onPermitLeader: () => {
        stopListenForVotes(node);
      },

      onTransition: transition => {
        logger.info(`${transition.transition}: ${transition.from} to ${transition.to}`);
      }
    }
  });

  node.fsm = fsm;
  node.provider = new Provider(node);

  return {
    init: () => fsm.initialize(),
    destroy: () => fsm.reset(),
    get state() { return fsm.state; }
  };
};

module.exports = {
  createNode
};
