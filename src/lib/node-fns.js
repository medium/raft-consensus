const randomize = ([min, max]) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

const startAnnouce = node => {
  const provider = node.provider;

  provider.announce();
  const announceInterval = setInterval(
    () => provider.announce(), node.options.announceInterval
  );

  node.announceInterval = announceInterval;
};

const initElectionTimout = node => {
  if (node.electionTimeout) clearTimeout(node.electionTimeout);

  node.electionTimeout = setTimeout(() => {
    if (node.fsm.state === 'candidate') {
      return node.fsm.loseElection();
    }
    if (node.fsm.state === 'follower') {
      return node.fsm.becomeCandidate();
    }
  }, randomize(node.options.electionTimeout));
};

const stopElectionTimeout = node => {
  if (node.electionTimeout) clearTimeout(node.electionTimeout);
};

const listenLeaderHeartbeat = node => {
  node.events.on('leaderHeartbeat', ({ term, nodeId }) => {
    if (node.fsm.state === 'leader' && node.term < term) {
      node.term = term;
      return node.fsm.admitLeader();
    }
    if (node.fsm.state === 'candidate') {
      return node.fsm.loseElection();
    }
    initElectionTimout(node);
  });
};

const stopListenLeaderHeartbeat = node => {
  node.events.removeAllListeners('leaderHeartbeat');
};

const listenVoteOffers = node => {
  node.events.on('voteOffer', ({ term, nodeId }) => {
    initElectionTimout(node);

    if (node.term < term) {
      node.votedFor = undefined;
      node.term = term;
    }

    if (!node.votedFor) {
      node.votedFor = nodeId;
      node.provider.applyVoteOffer(nodeId);
    }
    else {
      node.provider.rejectVoteOffer(nodeId);
    }
  });
};

const stopListenVoteOffers = node => {
  node.events.removeAllListeners('voteOffer');
};

const sendVoteOffers = node => {
  node.provider.sendVote();
};

const listenForVotes = node => {
  node.provider.getMembersCount((err, totalMembers) => {
    if (totalMembers === 1) node.fsm.becomeLeader();
  });

  const votes = { applied: 1, rejected: 0 };

  node.events.on('appliedOffer', () => {
    node.provider.getMembersCount((err, totalMembers) => {
      votes.applied++;
      const quorum = votes.applied / totalMembers;
      if (node.fsm.state === 'candidate' && quorum > 0.5) node.fsm.becomeLeader();
    });
  });

  node.events.on('rejectedOffer', () => {
    node.provider.getMembersCount((err, totalMembers) => {
      votes.rejected++;
      const quorum = votes.rejected / totalMembers;
      if (node.fsm.state === 'candidate' && quorum >= 0.5) node.fsm.loseElection();
    });
  });
};

const stopListenForVotes = node => {
  node.events.removeAllListeners('appliedOffer');
  node.events.removeAllListeners('rejectedOffer');
};

const initLeaderHeartbeat = node => {
  node.heartbeatInterval = setInterval(() => {
    node.provider.leaderHeartbeat();
  }, node.options.heartbeatInterval);
};

const newElectionTerm = node => {
  node.term++;
};

module.exports = {
  startAnnouce,
  initElectionTimout,
  stopElectionTimeout,
  initLeaderHeartbeat,
  sendVoteOffers,
  newElectionTerm,
  listenLeaderHeartbeat,
  listenVoteOffers,
  listenForVotes,
  stopListenLeaderHeartbeat,
  stopListenVoteOffers,
  stopListenForVotes
};
