const configParser = require('./lib/config-parser');

const redisConfig = configParser('./config/redis.yml');

module.exports = {
  redis: redisConfig,
};
