const { create, env } = require('sanctuary');

const S = create({ checkTypes: false, env: env });

module.exports = S;
